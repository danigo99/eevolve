# eVOLVE - Daniel Belo Goncalves CS Project

**eVolve** is an educational application for KS3 biology that allows students to interact with a natural selection simulator and observe how changing creature characteristics affects the evolution of the populations.

This is the final product from my third year project submitted on 11/05/2020.

## How to use
Download this directory and open the `./public/start.html` file. The only prerequisite is to have cross-origin content control disabled on your browser, see guides for each major browser here:

 - Safari: goto Safari -> Preferences -> Advanced -> ***Show Develop Menu in menu bar***.  In the menu bar, under developer, tick ***Disable Cross-Origin Restrictions***.
 - Chrome: [Install this Chrome extension](https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf?hl=en) and enable it functionality.
 - Firefox: [Install this Firefox extension](https://addons.mozilla.org/en-GB/firefox/addon/access-control-allow-origin/?src=search) and enable its functionality.

## Files

Here is a brief description of the files contained in this directory

 - `./imgs/` directory contains all the images needed for the website.
 - `./blender-files/` directory contains the MTL and OBJ files for the various 3D objects. These were modelled in *Blender* and are loaded by the *Three.js* loader.
 - `./graphs/` directory contains files to setup the dynamic graphs during the live simulation view.
 - `./threejs-loader/` contains the files to load MTL and OBJ files onto the renderer scene. These are provided by the *Three.js* library.
 - `./public/allInOne.html` and `./public/start.html` are the base HTML file that hold the application's skeleton and primary content.
 - `./public/Creature.js`, `./public/eventHandler.js`, `./public/exampleRender.js`, `./public/Food.js`, `./public/Population.js` and `./public/simRender.js` together contain the Javascript code to setup and run the renders and handle the simulation.

Note that detailed descriptions for the above are available in the header of each file.

## Built With

The development of this application relied on the use of the following libraries:
 - [Three.js](https://threejs.org/)
 - [Bootstrap](https://getbootstrap.com/)
 - [Chart.js](https://www.chartjs.org/)
 - [Chroma.js](https://gka.github.io/chroma.js/)