/**
* Object class which contains the creature objects and their attributes.
*/
class Creature {
  /*
    Name - the name of the object within the THREE scene. Used to reference the object and change its params after loading.
    Color - the color (RGB) of this creature
    Size - the size of this creature
    Speed - the speed of this creature
    Sight - how far they can see
    Life Expectancy - the average life expectancy of this creature in days. Note that this value doesn't concretely define how long an individual creature will live, rather the average for its population.
    StartPosX, StartPosZ - the coordinates where the creature will begin and end each day
  */
  
  /**
  * @desc constructor method for the object, setting all the necessary params.
  * @param color - colour of the creature, number speed, number sight, number startPosX, number startPosZ - both of
  *     which describe the coordinates where the creature will start and end each day.
  * @return void
  */
  constructor(oName, colour, size, speed, sight, lifeExp, startPosX, startPosZ){
    this.oName = oName;
    this.colour = colour;
    this.speed = speed;
    this.size = size;
    this.sight = sight;
    this.lifeExpectancy = lifeExp;
    this.startPosX = startPosX;
    this.startPosZ = startPosZ;
    this.currPosX = startPosX;
    this.currPosZ = startPosZ;
    
    this.ranCounter = 0;
    this.done = false;  // Flag indicates whether the creature is done for the day. Set to false initially
    this.setDailyFood();
  }
  
  getName(){
    return this.oName;
  }
  getColour(){
    return this.colour;
  }
  getSize(){
    return this.size;
  }
  getSpeed(){
    return this.speed;
  }
  getSight(){
    return this.sight;
  }
  getDailyFood(){
    return this.dailyFood;
  }
  getDailyFoodTotal(){
    return this.dailyFoodTotal;
  }
  getLifeExpectancy(){
    return this.lifeExpectancy;
  }
  getStartPosX(){
    return this.startPosX;
  }
  getStartPosZ(){
    return this.startPosZ;
  }
  getCurrPosX(){
    return this.currPosX;
  }
  getCurrPosZ(){
    return this.currPosZ;
  }
  getTargetX(){
    return this.targetX;
  }
  getTargetZ(){
    return this.targetZ;
  }
  getTargetType(){
    return this.targetType;
  }
  getRanCounter(){
    return this.ranCounter;
  }
  getDone(){
    return this.done;
  }
  
  getBearing(targetX, targetZ){
    var dz = targetZ - this.currPosZ;
    var dx = targetX - this.currPosX;
    var theta = Math.atan(dz/dx);
    //theta *= 180/Math.PI; // rads to degs
    
    var dot = this.currPosX*targetX + this.currPosZ*targetZ;      // dot product between [x1, y1] and [x2, y2]
    var det = this.currPosX*targetZ - this.currPosZ*targetX;      // determinant
    var angle = Math.atan2(det, dot);  // atan2(y, x) or atan2(sin, cos)
    return angle;
  }
  
  /**
  * @desc method returns straight line distance (in world view units) to the given point from the current position.
  * @param tx,tz - target x and z coordinates.
  * @return straight line distance to target from current pos.
  */
  getDistanceTo(tx,tz){
    var dx = this.currPosX - tx;    // Change in x
    var dz = this.currPosZ - tz;    // Change in z
    
    var dis = Math.sqrt(Math.pow(dx,2) + Math.pow(dz,2));   // Length of hypotenuse
    
    return dis;
  }
  
  /**
  * @desc method returns the coordinates of the next point one unit along the cartesian path to the target.
  * @param unit - the distance along which to move towards the target.
  * @return coordinates of the point to move to in the form [x,z].
  */
  getNextPoint(unit){
    //Translate points so target is at the origin
    var xn = this.currPosX - this.targetX;
    var zn = this.currPosZ - this.targetZ;
    
    var finalX;
    var finalZ;
    
    if (this.currPosX == this.targetX){   // X coordinate doesn't change
      finalX = this.targetX;
      if (this.currPosZ > this.targetZ){
        finalZ = this.currentPosZ - unit;
      }else if (this.currPosZ < this.targetZ){
        finalZ = this.currPosZ + unit;
      }else{
        finalZ = this.targetZ;
      }
    }else{
      var r = Math.sqrt(Math.pow(zn,2) + Math.pow(xn,2));
      var theta = Math.atan(zn/xn);
      
      var x = (r-unit) * Math.cos(theta);
      var z = (r-unit) * Math.sin(theta);
      
      finalX = this.targetX + x;
      finalZ = this.targetZ + z;
      
      var change = this.currPosX - this.targetX;
      
      if (change < 0){
        finalX = this.targetX - x;
        finalZ = this.targetZ - z;
      }
    }
    
    return [finalX, finalZ];
  }
  
  /**
  * @desc method returns the coordinates of the closest visible food item.
  * @param foods - list containing all of the available food objects.
  * @return coordinates of the point to move to in the form [x,z] or null if there is no food in sight of the creature.
  */
  getClosestFood(foods){
    // Create a list of all food objs the creature can see from its current position
    var possibles = [];
    var i;
    for (i=0; i<foods.length; i++){
      var distanceTo = this.getDistanceTo(foods[i].getPosX(), foods[i].getPosZ());
      if (distanceTo < this.sight){
        // The object is within sight, so the creature can see it.
        possibles.push([distanceTo,foods[i]]);
      }
    }
    
    if (possibles.length > 0){
      var best = [10000,"n"];
      for (i=0; i<possibles.length; i++){
        if (possibles[i][0] < best[0]){
          best = possibles[i];
        }
      }
      return [best[1].getPosX(),best[1].getPosZ()];
    }
    
    return null; // there is no food within sight.
  }
  
  /**
  * @desc Create a child of the current creature object. This includes the mutation to the current genome
  * @param nam - the name of the new child to be create 
  * @return new Creature object
  */
  mutate(nam){
    var params = [nam,this.colour,this.size,this.speed,this.sight,this.lifeExpectancy,this.startPosX,this.startPosX];
    let change;
    let chance;
    let ve;
    
    // Mutate the current genome with a 10% chance of mutation per gene
    for (var i=2; i<5; i++){
      chance = Math.random();
      if (chance <= 0.1){ // 10% chance of mutation for each gene
        var counter = 0;
        if (i==4){
          do{
            counter++;
            change = Math.random()/2;
            ve = Math.random();
            change = ((ve < 0.5) ? change+1 : 1-change);
            params[i] = params[i] * change;
          }while ((params[i]>5 || params[i]<=0) && counter < 20);
        }else if (i==3){
          do{
            counter++;
            change = Math.random()/2;
            ve = Math.random();
            change = ((ve < 0.5) ? change+1 : 1-change);
            params[i] = params[i] * change;
          }while ((params[i]>9 || params[i]<=0) && counter < 20);
        }else if (i==2){
          do{
            counter++;
            change = Math.random()/2;
            ve = Math.random();
            change = ((ve < 0.5) ? change+1 : 1-change);
            params[i] = params[i] * change;
          }while ((params[i]>3 || params[i]<1) && counter < 20);
        }
      }
    }
    
    // Create new random position on perimiter for the child's home location
    chance = Math.random();
    chance = ((chance < 0.5) ? 0 : 1);
    ve = Math.random();
    ve = ((ve < 0.5) ? -1 : 1);
    var ve2 = Math.random();
    ve2 = ((ve2 < 0.5) ? -1 : 1);
    var coords;
    if (chance==0){
      coords = [5.4*ve , ve2*(Math.random()*5.4)];
    }else{
      coords = [ve2*(Math.random()*5.4) , 5.4*ve];
    }
    
    // var spacing = 1;
//     if (params[6]==5.4){
//       // On the top part of the board
//       if (!(params[7] + 0.2 > 5.4)){
//         params[7] = params[7] + spacing;
//       }else{
//         params[7] = params[7] - spacing;
//       }
//     }else if (params[6]==-5.4){
//       // On the bottom part of the board
//       if (!(params[7] - 0.2 < -5.4)){
//         params[7] = params[7] - spacing;
//       }else{
//         params[7] = params[7] + spacing;
//       }
//     }else if (params[7]==5.4){
//       // On the right side of the board
//       if (!(params[6] + 0.2 > 5.4)){
//         params[6] = params[6] + spacing;
//       }else{
//         params[6] = params[6] - spacing;
//       }
//     }else if (params[7]==-5.4){
//       // On the left side of the board
//       if (!(params[6] - 0.2 < -5.4)){
//         params[6] = params[6] - spacing;
//       }else{
//         params[6] = params[6] + spacing;
//       }
//     }
//     console.log("Child position "+params[6]+","+params[7]);
    return (new Creature (params[0], params[1], params[2], params[3], params[4], params[5], coords[0], coords[1]));
  }
  
  /**
  * @desc Use the DailyFoodRequirements formula to calculate the amount of food the creature needst to survive a day.
  * @param void
  * @return void
  */
  setDailyFood(){
    // Set it to 2x the food requirements as this is the reproductive requirement
    this.dailyFood = 2*Math.ceil(((Math.pow(this.size,2)*this.speed)+this.sight) / 20);
    this.dailyFoodTotal = this.dailyFood;
  }
  
  /**
  * @desc Update the current food requirements by decrementing the variable.
  * @param void
  * @return new current food requirements after update
  */
  updateDailyFood(){
    this.dailyFood = this.dailyFood - 1;
    return this.dailyFood;
  }
  
  // Set colour of the creature to the parameter.
  setColour(colour){
    this.colour = colour;
  }
  // Set size of the creature to the parameter.
  setSize(size){
    this.size = size;
  }
  // Set speed of the creature to the parameter.
  setSpeed(speed){
    this.speed = speed;
  }
  // Set sight of the creature to the parameter.
  setSight(sight){
    this.sight = sight;
  }
  // Set life expectancy of the creature to the parameter.
  setLifeExpectancy(lifeExp){
    this.lifeExpectancy = lifeExp;
  }
  // Set starting position of the creature to the parameter.
  setStartPosX(pos){
    this.startPosX = pos;
  }
  setStartPosZ(pos){
    this.startPosZ = pos;
  }
  // Set current position of the creature to the parameter.
  setCurrPosX(pos){
    this.currPosX = pos;
  }
  setCurrPosZ(pos){
    this.currPosZ = pos;
  }
  
  /**
  * @desc Set target position of the creature to the parameter (in the form [x,z]) and set the type of target
  * @param coord - coordinates of the target
           type - type of target: 0 = Food, 1 = Home, 2 = Random point
  * @return void
  */
  setTarget(coord,type){
    this.targetX = coord[0];
    this.targetZ = coord[1];
    this.targetType = type;
  }
  
  // Set Done to mark if the creature is done for the day or not
  setDone(done){
    this.done = done;
  }
  
  // Count number of times creature has gone to a new random coordinate
  incRanCounter(){
    this.ranCounter ++;
  }
  resetRanCounter(){
    this.ranCounter = 0;
  }
}