/**
* Object class which contains the population objects and their attributes.
*/
class Population {
  /*
  * name - name of the population
  * popSize - size of the population (number of creatures)
  * speed - speed variable of the creatures of this population
  * size - size variable of the creatures of this population
  * sight - sight variable of the creatures of this population
  */
  
  /**
  * @desc Constructor method for the object, setting all the necessary params.
  * @param params stated above to initialise object
  * @return void
  */
  constructor(name,popSize,speed,size,sight){
    this.name = name;
    this.popSize = popSize;
    this.speed = speed;
    this.size = size;
    this.sight = sight;
  }
  
  getName(){
    return this.name;
  }
  getPopSize(){
    return this.popSize;
  }
  getSpeed(){
    return this.speed;
  }
  getSize(){
    return this.size;
  }
  getSight(){
    return this.sight;
  }
  
  setPopSize(popSize){
    this.popSize = popSize;
  }
  setSpeed(speed){
    this.speed = speed;
  }
  setSize(size){
    this.size = size;
  }
  setSight(sight){
    this.sight = sight;
  }
}