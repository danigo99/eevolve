/*
*   File handles the Size graph during the live simulation
*/

var ctx = document.getElementById('sizeChart');
var scale = chroma.scale("OrRd").domain([1,3]);
var sizeChart = new Chart(ctx, {
    type: 'bar',
    data: {
        // Setup labels and intial data (default)
        labels: ['1', '1.25', '1.5', '1.75', '2', '2.25', '2.5', '2.75', '3'],
        datasets: [{
            label: '# of Creatures',
            data: [0,0,0,0,0,0,0,0,0],    // data variable will be changed dynamically
            backgroundColor: [
                scale(1).hex(),
                scale(1.25).hex(),
                scale(1.5).hex(),
                scale(1.75).hex(),
                scale(2).hex(),
                scale(2.25).hex(),
                scale(2.5).hex(),
                scale(2.75).hex(),
                scale(3).hex(),
            ]
        }]
    },
    
    // Set options for the graph
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          },
          scaleLabel:{
            display: true,
            labelString: "Number of Creatures",
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          }
        }],
        xAxes: [{
          gridLines: {
            display: false
          },
          ticks: {
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          },
          scaleLabel:{
            display: true,
            labelString: "Size",
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          }
        }]
      },
      legend:{
        display:false
      }
    }
});