/*
*   File handles the Sight graph during the live simulation
*/

var ctx = document.getElementById('sightChart');
var scale = chroma.scale("OrRd").domain([0,5]);
var sightChart = new Chart(ctx, {
    type: 'bar',
    data: {
        // Setup labels and initial data (default)
        labels: ['0', '0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5'],
        datasets: [{
            label: '# of Creatures',
            data: [0,0,0,0,0,0,0,0,0,0,0],    // data variable will be changed dynamically
            backgroundColor: [
                scale(0).hex(),
                scale(0.5).hex(),
                scale(1).hex(),
                scale(1.5).hex(),
                scale(2).hex(),
                scale(2.5).hex(),
                scale(3).hex(),
                scale(3.5).hex(),
                scale(4).hex(),
                scale(4.5).hex(),
                scale(5).hex()
            ]
        }]
    },
    
    // Set options for the graph
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          },
          scaleLabel:{
            display: true,
            labelString: "Number of Creatures",
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          }
        }],
        xAxes: [{
          gridLines: {
            display: false
          },
          ticks: {
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          },
          scaleLabel:{
            display: true,
            labelString: "Sight",
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          }
        }]
      },
      legend:{
        display:false
      }
    }
});