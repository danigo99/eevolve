/*
*   File handles the Speed graph during the live simulation
*/

var ctx = document.getElementById('speedChart');
var scale = chroma.scale("Accent").domain([0,9]);
var speedChart = new Chart(ctx, {
    type: 'bar',
    data: {
        // Setup labels and intial data (default)
        labels: ['0', '0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5', '5.5', '6', '6.5', '7', '7.5', '8', '8.5', '9'],
        datasets: [{
            label: '# of Creatures',
            data: [0, 0, 0, 0, 0, 0,0,0,0,0,0,0,0,0,0,0,0,0],    // data variable will be changed dynamically
            backgroundColor: [
                scale(0).hex(),
                scale(0.5).hex(),
                scale(1).hex(),
                scale(1.5).hex(),
                scale(2).hex(),
                scale(2.5).hex(),
                scale(3).hex(),
                scale(3.5).hex(),
                scale(4).hex(),
                scale(4.5).hex(),
                scale(5).hex(),
                scale(5.5).hex(),
                scale(6).hex(),
                scale(6.5).hex(),
                scale(7).hex(),
                scale(7.5).hex(),
                scale(8).hex(),
                scale(8.5).hex(),
                scale(9).hex()
            ]
        }]
    },
    
    // Set options for the graph
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          },
          scaleLabel:{
            display: true,
            labelString: "Number of Creatures",
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          }
        }],
        xAxes: [{
          gridLines: {
            display: false
          },
          ticks: {
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          },
          scaleLabel:{
            display: true,
            labelString: "Speed",
            fontSize: 20,
            fontFamily: 'Avenir Next',
            fontColor: '#ffffff'
          }
        }]
      },
      legend:{
        display:false
      }
    }
});