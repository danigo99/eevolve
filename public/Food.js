/**
* Object class which contains the food objects and their attributes.
*/
class Food{
  /**
  * @desc constructor method for the object, setting all the necessary params.
  * @param posX, posZ - the position of the food object on the plane.
  * @return void
  */
  constructor(posX, posZ, fname){
    this.posX = posX;
    this.posZ = posZ;
    this.name = fname;
  }
  
  // Return the position of food object
  getPosX(){
    return this.posX;
  }
  getPosZ(){
    return this.posZ;
  }
  
  getName(){
    return this.name;
  }
}