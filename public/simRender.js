let floor;
let creature;
let creatureCount;
let index;
let files;

var doneLoading = false;  // Flag indicates whether all the objects are done loading to the scene
var doneLoadingFood = true;
var shouldRes = true;
var dayCounter = 0;
var pause = false;
var normalDist = true;

var maxRandomMoves = 2;
var numFoodItems = 50;

var targetX = 0;
var targetZ = 0;


const scene = new THREE.Scene();

const light_intensity = 0.3;
const light1 = new THREE.DirectionalLight('#FFDBC4', light_intensity);
light1.position.set(-10, 20, 10);
const light2 = new THREE.DirectionalLight('#BAB9FF', light_intensity);
light2.position.set(-10, 20, -10);
const light3 = new THREE.DirectionalLight('#CBEBFF', light_intensity);
light3.position.set(10, 20, -10);
const light4 = new THREE.DirectionalLight('#E2FFD2', light_intensity);
light4.position.set(10, 20, 10);


scene.add(light1);
scene.add(light2);
scene.add(light3);
scene.add(light4);

const camera = new THREE.PerspectiveCamera(45, 700/500, 0.1, 1000);
camera.position.x = 10;
camera.position.y = 4;
camera.position.z = -10;
camera.lookAt(2,0,-2);

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(1000, 700);
document.getElementById("simulation").appendChild(renderer.domElement);

const objLoader = new THREE.OBJLoader();
objLoader.setPath('./blender-files/');

const mtlLoader = new THREE.MTLLoader();
mtlLoader.setPath('./blender-files/');

/**
* @desc Generates a Hex colour for the given speed. Used to colour the creature objects
* @param speed - speed of the creature to be coloured
* @return Hex colour
*/
function getHexColor(speed){
  var scale = chroma.scale("Accent").domain([0,9]);

  return scale(speed).hex();
}

/**
* @desc Function moves the given object from a point to another
* @param object - the object which is being moved. 
*        x,z - the coordinates of the point to which we want to move.
*        x0,z0 - the coordinates of th  points to which we want to move.
* @return void
*/
function applyTranslate(object, x, x0, z, z0){
  var xn = x - x0;
  var zn = z - z0;

  object.translateZ(zn);
  object.translateX(xn);
  return 0;
}

/**
* @desc Function rotate the given object by the given angle
* @param object - mesh object to rotate
         angle - angle by whicht he object should rotate
* @return void
*/
function applyRotate(object, angle){
  object.rotateY(angle);
  return 0;
}

/**
* @desc Generates a random coordinate, either normally or uniformly distributed depending on environment variable
* @param void
* @return x,z - random coordinate
*/
function randomCoord(){
  if (normalDist){
    // Use the Box-Muller transform to create normally distributed numbers with mu=0 and sigma=2.
    var mu = 0;
    var sigma = 3;
    var u = Math.random();
    var v = Math.random();

    var x1 = Math.sqrt(-2 * Math.log10(u)) * Math.sin(2 * Math.PI * v);
    var z1 = Math.sqrt(-2 * Math.log10(u)) * Math.cos(2 * Math.PI * v);

    var x = mu + x1 * sigma;
    var z = mu + z1 * sigma;
  }else{
    // Generate randomly distribtued coordinate
    var ve = Math.random();
    ve = ((ve < 0.5) ? -1 : 1);
    
    var ve2 = Math.random();
    ve2 = ((ve < 0.5) ? -1 : 1);
    
    var x = ve*Math.random()*5.4;
    var z = ve2*Math.random()*5.4;
  }
  

  return [x,z];
}

// Array holds the list of creatures that need to be displayed on the environment. Use for testing
// creatures = [ new Creature("creature0",3,2,5,2.5,10,-5.4,-5.4),
//               new Creature("creature1",3,2,5,2.5,10,5.4,5.4),
//               new Creature("creature2",3,2,5,2.5,10,-5.4,5.4)
//             ];

function simGo(){
  // Array holds the list of available food 
  foods = [];
  numFoodItems = amtFood;
  document.getElementById("foodPDay").innerHTML = 'Amount of food available per day: '+numFoodItems+'';
  for (i=0; i<numFoodItems; i++){
    var randCoord = randomCoord();
    foods.push(new Food(randCoord[0],randCoord[1],"food"+i));    // Add new food item to the list
  }
  
  files = ['floor'];
  index = 0;
  creatureCount = creatures.length;
  for (i=0; i<creatures.length; i++){
    files.push(creatures[i]);
  }
  for (i=0; i<foods.length; i++){
    files.push(foods[i]);
  }
  loadNextFile();   // Start loading objects to the scene
  render();         // Prepare for render
}
      
// var creatureCount = 4;

/**
* @desc Callback funciton loads all the object files fron the files array.
* @param void
* @return void
*/
function loadNextFile() {
  if (index > files.length - 1){
    doneLoading = true;
    return; // If there are no more files to load then break from the recursion.
  }

  if (index == 0){
    // Adding the floor obj
    materialsFile = "floor";
    objFile = "floor";
  }else if (index <= (creatures.length)){
    // Adding a creature obj
    materialsFile = "creature";
    objFile = "creature";
  }else{
    // Adding a food obj
    materialsFile = "food";
    objFile = "food";
  }

  mtlLoader.load(materialsFile+'.mtl', function(materials){
    materials.preload();
    new THREE.OBJLoader()
      .setMaterials(materials)
      .setPath('./blender-files/')
      .load(objFile+'.obj', function ( group ) {
  
        mesh = group.children[0];
        mesh.material.side = THREE.DoubleSide;
        mesh.castShadow = true;
  
        if (index == 0){
          // Adding the floor obj
          mesh.name = "floor";
          floor = mesh;
        }else if (index <= (creatures.length)){
          // Adding a creature obj
          mesh.name = creatures[(index-1)].getName();
          var size = creatures[(index-1)].getSize();
          mesh.translateY(0.3);
          mesh.scale.set(1.2*size,1.2*size,1.2*size);
          mesh.rotateY(3.14);
    
          // Set the first target to the closest food item or a random point
          var closestFood = creatures[(index-1)].getClosestFood(foods);
          if (closestFood == null){
            // Go to a random point (more distributed in centre) on the board.
            var randPos = randomCoord();
            creatures[(index-1)].setTarget(randPos, 2);
            creatures[(index-1)].incRanCounter();
          }else{
            // Go to the closest food item.
            creatures[(index-1)].setTarget(closestFood, 0);
          }
          
          mesh.material[0].color.setStyle(getHexColor(creatures[(index-1)].getSpeed()));
    
          applyTranslate(mesh, creatures[(index-1)].getStartPosX(), 0, creatures[(index-1)].getStartPosZ(), 0);
        }else{
          // Adding a food obj
          mesh.name = foods[(index-1-creatures.length)].getName();
          mesh.translateY(0.3);
          // TODO: Workout why the coords need to be fliped. Might be something that needs to be sorted out
          applyTranslate(mesh, -foods[(index-1-creatures.length)].getPosX(), 0, -foods[(index-1-creatures.length)].getPosZ(), 0);
        }
  
        scene.add(mesh);  // Add the object to the scene

        index++; // incrememnt count and load the next OBJ
        loadNextFile();

      });
  });
}

//loadNextFile();     // Call callback function to start loading the objects use for debugging

/**
* @desc Function to determine whether the given coordinates has a food object on it.
* @param x,z - coordinates to check
* @return true/false - exists/doesn't exist
*/
function foodExists(x,z){
  var f;
  var exists = false;
  for (f=0;f<foods.length;f++){
    if (foods[f].getPosX()==x && foods[f].getPosZ()==z){
      exists = true;
    }
  }
  return exists;
}

/**
* @desc Function loads all of thef food objects at the start of each day.
* @param newFoods - list of remaining food objects to be loaded
* @return void
*/
function loadFoods(newFoods){
  if (newFoods.length == 0){
    // No more objects to load. Set flag and break recursive loop
    doneLoadingFood = true;
    return;
  }

  var currFood = newFoods.pop();

  mtlLoader.load('food.mtl', function(materials){
    materials.preload();
    new THREE.OBJLoader()
      .setMaterials(materials)
      .setPath('./blender-files/')
      .load('food.obj', function (group) {
        mesh = group.children[0];
        mesh.material.side = THREE.DoubleSide;
        mesh.castShadow = true;

        // Adding a food obj
        mesh.name = currFood.getName();
        mesh.translateY(0.3);
        applyTranslate(mesh, -currFood.getPosX(), 0, -currFood.getPosZ(), 0);            

        scene.add(mesh);  // Add the object to the scene

        index++; // incrememnt count and load the next OBJ
  
        // Load next food object in list
        foods.push(currFood);
        loadFoods(newFoods);
      });
  });
}

/**
* @desc Function loads the new creatures to the scene at the end of each day.
* @param newChildren - list of remaining creature objects to be loaded
* @return void
*/
function loadChildren(newChildren){
  if (newChildren.length == 0){ // Done loading the new children to the scene
    doneLoading = true;
    return;
  }

  var currChild = newChildren.pop();  // Get the next child to be loaded

  mtlLoader.load('creature.mtl', function(materials){
    materials.preload();
    new THREE.OBJLoader()
      .setMaterials(materials)
      .setPath('./blender-files/')
      .load('creature.obj', function (group) {
        mesh = group.children[0];
        mesh.material.side = THREE.DoubleSide;
        mesh.castShadow = true;

        // Adding a creature obj
        mesh.name = currChild.getName();
        var size = currChild.getSize();
        mesh.translateY(0.3);
        mesh.scale.set(1.2*size,1.2*size,1.2*size);
        mesh.rotateY(3.14);

        // Set the first target to the closest food item or a random point
        var closestFood = currChild.getClosestFood(foods);
        if (closestFood == null){
          // Go to a random point (more distributed in centre) on the board.
          var randPos = randomCoord();
          currChild.setTarget(randPos, 2);
          currChild.incRanCounter();
        }else{
          // Go to the closest food item.
          currChild.setTarget(closestFood, 0);
        }
        
        console.log("Adding child at "+currChild.getStartPosX()+","+currChild.getStartPosZ());
        applyTranslate(mesh, currChild.getStartPosX(), 0, currChild.getStartPosZ(), 0);
        
        mesh.material[0].color.setStyle(getHexColor(currChild.getSpeed()));
  

        scene.add(mesh);  // Add the object to the scene

        index++; // incrememnt count and load the next OBJ
  
        creatures.push(currChild);
        loadChildren(newChildren);
      });
  });
}

/**
* @desc Update the given chart with new data
* @param chart - pointer to the chart object to update
         data - array containing the new data to be added to the chart
* @return void
*/
function updateChart(chart, data){
  for (var i=0;i<data.length;i++){
    chart.data.datasets[0].data[i] = data[i];
  }
  chart.update();
}

/**
* @desc Prepare histogram data for the speed graph.
* @param void
* @return prepared histogram
*/
function speedHist(){
  var hist = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  for (var i=0;i<creatures.length;i++){
    var sp = creatures[i].getSpeed();
    if (sp < 0.5){
      hist[0]++;
    }else if (sp < 1){
      hist[1]++;
    }else if (sp < 1.5){
      hist[2]++;
    }else if (sp < 2){
      hist[3]++;
    }else if (sp < 2.5){
      hist[4]++;
    }else if (sp < 3){
      hist[5]++;
    }else if (sp < 3.5){
      hist[6]++;
    }else if (sp < 4){
      hist[7]++;
    }else if (sp < 4.5){
      hist[8]++;
    }else if (sp < 5){
      hist[9]++;
    }else if (sp < 5.5){
      hist[10]++;
    }else if (sp < 6){
      hist[11]++;
    }else if (sp < 6.5){
      hist[12]++;
    }else if (sp < 7){
      hist[13]++;
    }else if (sp < 7.5){
      hist[14]++;
    }else if (sp < 8){
      hist[15]++;
    }else if (sp < 8.5){
      hist[16]++;
    }else if (sp < 9){
      hist[17]++;
    }
  }
  return hist;
}

/**
* @desc Prepare histogram data for the size graph.
* @param void
* @return prepared histogram
*/
function sizeHist(){
  var hist = [0,0,0,0,0,0,0,0,0];
  for (var i=0;i<creatures.length;i++){
    var sp = creatures[i].getSize();
    if (sp < 1.25){
      hist[0]++;
    }else if (sp < 1.5){
      hist[1]++;
    }else if (sp < 1.75){
      hist[2]++;
    }else if (sp < 2){
      hist[3]++;
    }else if (sp < 2.25){
      hist[4]++;
    }else if (sp < 2.5){
      hist[5]++;
    }else if (sp < 2.75){
      hist[6]++;
    }else if (sp < 3){
      hist[7]++;
    }
  }
  return hist;
}

/**
* @desc Prepare histogram data for the sight graph.
* @param void
* @return prepared histogram
*/
function sightHist(){
  var hist = [0,0,0,0,0,0,0,0,0,0,0];
  for (var i=0;i<creatures.length;i++){
    var sp = creatures[i].getSight();
    if (sp < 0.5){
      hist[0]++;
    }else if (sp < 1){
      hist[1]++;
    }else if (sp < 1.5){
      hist[2]++;
    }else if (sp < 2){
      hist[3]++;
    }else if (sp < 2.5){
      hist[4]++;
    }else if (sp < 3){
      hist[5]++;
    }else if (sp < 3.5){
      hist[6]++;
    }else if (sp < 4){
      hist[7]++;
    }else if (sp < 4.5){
      hist[8]++;
    }else if (sp < 5){
      hist[9]++;
    }
  }
  return hist;
}

/**
* @desc Button handler for the pause and play buttons.
* @param void
* @return void
*/
function pausePlaySim(){
  if (pause){
    // Clicked play
    pause = false;
    document.getElementById("pausePlayBtn").innerHTML = '<span class="glyphicon glyphicon-pause pull-right" style="font-size:100px; cursor:pointer;" onclick="pausePlaySim()"></span>';
  }else{
    // Clicked pause
    pause = true;
    document.getElementById("pausePlayBtn").innerHTML = '<span class="glyphicon glyphicon-play pull-right" style="font-size:100px; cursor:pointer;" onclick="pausePlaySim()"></span>';
  }
}

/**
* @desc Button handler for the stop button.
* @param void
* @return void
*/
function stopSim(){
  pause = true;
  // TODO: stop the simulation and show results page
}

// TODO: method description
function resetCreatures(){
  var creatureSpacing = 10.8/(creatures.length/4);
  var sideCounter = 1;
  var currStartX = 5.4;
  var currStartZ = -5.4;
  for (var c=0; c<creatures.length; c++){
    creatures[c].setStartPosX(currStartX);
    creatures[c].setCurrPosX(currStartX);
    creatures[c].setStartPosZ(currStartZ);
    creatures[c].setCurrPosZ(currStartZ);
    if (sideCounter==1){
      if (eval(currStartZ)+eval(creatureSpacing) > 5.4){
        currStartZ = eval(5.4);
        currStartX = 5.4-creatureSpacing;
        sideCounter ++;
      }else{
        currStartZ = eval(currStartZ) + eval(creatureSpacing);
      }
    }else if (sideCounter==2){
      if (currStartX-creatureSpacing < -5.4){
        currStartX = eval(-5.4);
        currStartZ = 5.4-creatureSpacing;
        sideCounter ++;
      }else{
        currStartX = eval(currStartX) - eval(creatureSpacing);
      }
    }else if (sideCounter==3){
      if (currStartZ-creatureSpacing < -5.4){
        currStartX = -5.4+eval(creatureSpacing);
        currStartZ = eval(-5.4);
        sideCounter ++;
      }else{
        currStartZ = eval(currStartZ) - eval(creatureSpacing);
      }
    }else if (sideCounter==4){
      if (eval(currStartX)+eval(creatureSpacing) > 5.4){
        currStartX = eval(5.4);
        currStartZ = -5.4+eval(creatureSpacing);
      }else{
        currStartX = eval(currStartX) + eval(creatureSpacing);
      }
    }
    var creat = scene.getObjectByName(creatures[c].getName());
    //applyTranslate(creat, creatures[c].getStartPosX(), creatures[c].getCurrPosX(), creatures[c].getStartPosZ(), creatures[c].getCurrPosZ());
    
    console.log("reseting: "+creatures[c].getStartPosX()+","+creatures[c].getStartPosZ())
  }
  shouldRes = false;
}

// TODO: method description
function render() {
  if (doneLoading && doneLoadingFood && !pause) {
    document.getElementById("numCreatureLabel").innerHTML = "Total number of creatures: "+creatures.length;
    
    // Check if all of the creatures are done for the day
    var allDone = true;
    for (var i=0; i<creatures.length; i++){
      allDone = allDone && creatures[i].getDone();
    }
    
    if (shouldRes){
      //resetCreatures();
    }
    
    if (!allDone){
      for (var i=0; i<creatures.length; i++){
        var object = scene.getObjectByName(creatures[i].getName());
        var speed = creatures[i].getSpeed();
        speed = speed / 10;

        var tx = creatures[i].getTargetX();
        var tz = creatures[i].getTargetZ();
        var cx = creatures[i].getCurrPosX();
        var cz = creatures[i].getCurrPosZ();

        if (tx!=cx || tz!=cz){ // In this case, the creature hasn't reached its target yet
          if (creatures[i].getDistanceTo(tx,tz) > speed){
    
            var targetType = creatures[i].getTargetType();
            if (targetType == 0){
              // Target is currently a food item
              // Check if this still exists (not eaten by another creature in the mean time)
              if (!foodExists(tx,tz)){
                // Then the food object we are looking for no longer exists. Look for more food.
                var nextFood = creatures[i].getClosestFood(foods);
                if (nextFood == null){
                  if (creatures[i].getRanCounter() != maxRandomMoves){
                    // Go to a random point (more distributed in centre) on the board.
                    creatures[i].incRanCounter();
                    var randPos = randomCoord();
                    creatures[i].setTarget(randPos, 2);
                  }else{
                    // Creature has been looking for food unsucsessfuly for too long
                    creatures[i].setDone(true);
                  }
                }else{
                  // Move towards the food item found
                  creatures[i].setTarget(nextFood, 0);
                }
              }
      
            }else if (targetType==2){
              // Target is currently a random point. Check if there are any food objs close enough to see.
              var nextFood = creatures[i].getClosestFood(foods);
              if (nextFood != null){
                // Move towards the food item found
                creatures[i].setTarget(nextFood, 0);
              }
              // If nothing is found then continue towards the last chosen random point
            }
    
            var tuple = creatures[i].getNextPoint(speed);   // Get the coordinates of the next point the creature should move to.
            applyTranslate(object, tuple[0], cx, tuple[1], cz);
            creatures[i].setCurrPosX(tuple[0]);
            creatures[i].setCurrPosZ(tuple[1]);
    
          }else{
            // Creature is close enough to target to just jump straight to it.
            // TODO: do this in the CREATURE object before return.
            applyTranslate(object, tx, cx, tz, cz);
            creatures[i].setCurrPosX(tx);
            creatures[i].setCurrPosZ(tz);
          }
        }else{
          // The creature has reached its target
          sx = creatures[i].getStartPosX();
          sz = creatures[i].getStartPosZ();
  
          var targetType = creatures[i].getTargetType();
  
          // Targets can only be the creature's home coordinate, a food object or a random point.
          if (targetType == 1){
            // The target which was just reached was the creature's starting coordinate (home point).
            creatures[i].resetRanCounter();
            creatures[i].setDone(true);   // Signal that the creature is done for the day.
          }else if (targetType == 0){
            if (foodExists(tx,tz)){
              // The target which was just reached is a food item which still exists
              creatures[i].resetRanCounter();
              var currFoodNeeds = creatures[i].updateDailyFood();
              // Delete food object
              var d;
              var updFoods = [];
              for (d=0; d<foods.length; d++){
                // Keep all the food objects that weren't eaten
                if (foods[d].getPosX()!=tx && foods[d].getPosZ()!=tz){
                  updFoods.push(foods[d]);
                }else{
                  var objToDelete = scene.getObjectByName(foods[d].getName());
                  scene.remove(objToDelete);
                  objToDelete.geometry.dispose();
                  objToDelete.material.dispose();
                  objToDelete = undefined;
                }
              }
              foods = updFoods;
              if (currFoodNeeds <= 0){
                // Head home.
                creatures[i].setTarget([sx,sz], 1);
              }else{
                // Look for more food.
                var nextFood = creatures[i].getClosestFood(foods);
                if (nextFood == null){
                  // Go to a random point (more distributed in centre) on the board.
                  creatures[i].incRanCounter();
                  var randPos = randomCoord();
                  creatures[i].setTarget(randPos, 2);
                }else{
                  // Move towards the food item found
                  creatures[i].setTarget(nextFood, 0);
                }
              }
            }else{
              // Reached target which was a food item but no long exists
              var nextFood = creatures[i].getClosestFood(foods);
              if (nextFood == null){
                if (creatures[i].getRanCounter() != maxRandomMoves){
                  // Go to a random point (more distributed in centre) on the board.
                  creatures[i].incRanCounter();
                  var randPos = randomCoord();
                  creatures[i].setTarget(randPos, 2);
                }else{
                  // Creature has been looking for food unsucsessfuly for too long
                  creatures[i].setDone(true);
                }
              }else{
                // Move towards the food item found
                creatures[i].setTarget(nextFood, 0);
              }
            }
          }else /* The target which was just reached is a random point. Look again for closest food */ {
            if (creatures[i].getRanCounter() != maxRandomMoves){
              var nextFood = creatures[i].getClosestFood(foods);
              if (nextFood == null){
                // Go to a random point (more distributed in centre) on the board.
                creatures[i].incRanCounter();
                var randPos = randomCoord();
                creatures[i].setTarget(randPos, 2);
              }else{
                // Move towards the food item found
                creatures[i].setTarget(nextFood, 0);
              }
            }else{
              // Creature has been looking for food unsucsessfuly for too long
              creatures[i].setDone(true);
            }
          }
        }          
      }
    }else{
      // End of the day. Remove dead creatures and mutate new ones.
      dayCounter ++;
      document.getElementById("dayLabel").innerHTML = "Day "+dayCounter;
      
      updateChart(speedChart,speedHist());
      updateChart(sizeChart,sizeHist());
      updateChart(sightChart,sightHist());
      
      for (var f=0; f<foods.length; f++){
        var fdObj = scene.getObjectByName(foods[f].getName());
        if (fdObj){
          scene.remove(fdObj);
          fdObj.geometry.dispose();
          fdObj.material.dispose();
          fdObj = undefined;
        }
      }
      
      var newCreatures = [];

      var newFoods = [];
      foods = [];
      for (i=0; i<numFoodItems; i++){
        var randCoord = randomCoord();
        var newFood = new Food (randCoord[0],randCoord[1],"food"+i);
        newFoods.push(newFood);    // Add new food item to the list
        //foods.push(newFood);
      }
      console.log("Adding "+newFoods.length+" foods");
      doneLoadingFood = false;
      loadFoods(newFoods);

      var children = [];  // Array holds the next generation of creatures which have been generated.

      for (i=0; i<creatures.length; i++){
        if (creatures[i].getDailyFood() > creatures[i].getDailyFoodTotal()/2){
          var objToDelete = scene.getObjectByName(creatures[i].getName());
          scene.remove(objToDelete);
        }else{
          if (creatures[i].getDailyFood()==0){
            var newCreature = creatures[i].mutate("creature"+creatureCount);
            children.push(newCreature);
            creatureCount++;
          }
          // Set the first target to the closest food item or a random point
          var closestFood = creatures[i].getClosestFood(foods);
          if (closestFood == null){
            // Go to a random point (more distributed in centre) on the board.
            var randPos = randomCoord();
            creatures[i].setTarget(randPos, 2);
            creatures[i].incRanCounter();
          }else{
            // Go to the closest food item.
            creatures[i].setTarget(closestFood, 0);
          }
          
          newCreatures.push(creatures[i]);
          creatures[i].setDone(false);
          creatures[i].setDailyFood();
          creatures[i].resetRanCounter();
        }
      }

      creatures = newCreatures; // Update the creatures array.
      shouldRes = true;

      doneLoading = false;
      loadChildren(children);
    }
  }
  
  requestAnimationFrame(render);
  renderer.setClearColor(0x162521, 1);
  renderer.render(scene, camera);
}

// Uncomment for debugging
//simGo();