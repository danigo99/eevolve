// File handles the rendering of the creature example in the population creation page

// Starting default values
var currSpeed = 5;
var currSize = 2;
var currSight = 2.5;

// Setup Three.js scene and lighting
const exampleScene = new THREE.Scene();
const ex_light_intensity = 0.3;
const ex_light1 = new THREE.DirectionalLight('#FFDBC4', ex_light_intensity);
ex_light1.position.set(-10, 20, 10);
const ex_light2 = new THREE.DirectionalLight('#BAB9FF', ex_light_intensity);
ex_light2.position.set(-10, 20, -10);
const ex_light3 = new THREE.DirectionalLight('#CBEBFF', ex_light_intensity);
ex_light3.position.set(10, 20, -10);
const ex_light4 = new THREE.DirectionalLight('#E2FFD2', ex_light_intensity);
ex_light4.position.set(10, 20, 10);
exampleScene.add(ex_light1);
exampleScene.add(ex_light2);
exampleScene.add(ex_light3);
exampleScene.add(ex_light4);

// Setup Three.js camera
const ex_camera = new THREE.PerspectiveCamera(45, 1000/450, 0.1, 1000);
ex_camera.position.x = 5;
ex_camera.position.y = 4;
ex_camera.position.z = -5;
ex_camera.lookAt(2,2.5,-2);

// Setup Three.js rendered in given div
const ex_renderer = new THREE.WebGLRenderer({ antialias: true });
ex_renderer.setSize(1000, 450);
document.getElementById("exampleRender").appendChild(ex_renderer.domElement);

// Setup material and object loader files
const ex_mtlLoader = new THREE.MTLLoader();
ex_mtlLoader.setPath('./blender-files/');
const ex_objLoader = new THREE.OBJLoader();
ex_objLoader.setPath('./blender-files/');

/**
* @desc Generates a Hex colour for the given speed. Used to colour the creature objects.
* @param speed - speed of the creature to be coloured
* @return Hex colour
*/
function getHexColor(speed){
  var scale = chroma.scale("Accent").domain([0,9]);
  return scale(speed).hex();
}

/**
* @desc Function rotate the given object by the given angle
* @param object - mesh object to rotate
         angle - angle by whicht he object should rotate
* @return void
*/
function applyRotate(object, angle){
  object.rotateY(angle);
  return 0;
}

/**
* @desc Function calculates the food requirements given the current state of the variables.
* @param void
* @return food requirements
*/
function calcFoods(){
  var reqs = 2*Math.ceil(((Math.pow(currSize,2)*currSpeed)*eval(currSight)) / 20);

  return reqs;
}

/**
* @desc Update the parameters of the example creature object. Called when the variables are adjusted with range sliders
* @param void
* @return void
*/
function updateParams(){
  currSize = document.getElementById("sizeSlider").value;
  currSpeed = document.getElementById("speedSlider").value;
  currSight = document.getElementById("sightSlider").value;
  exampleScene.getObjectByName("exampleCreature").material[0].color.setStyle(getHexColor(currSpeed));
  exampleScene.getObjectByName("exampleCreature").scale.set(10*currSize/2,10*currSize/2,10*currSize/2);
}

/**
* @desc Callback funciton loads all the object files fron the files array.
* @param void
* @return void
*/
function exLoadNextFile() {
  ex_mtlLoader.load('creature.mtl', function(materials){
    materials.preload();
    new THREE.OBJLoader()
      .setMaterials(materials)
      .setPath('./blender-files/')
      .load('creature.obj', function ( group ) {
  
        mesh = group.children[0];
        mesh.material.side = THREE.DoubleSide;
        mesh.castShadow = true;
  
        // Adding a creature obj
        mesh.name = "exampleCreature";
        mesh.translateY(0.3);
        mesh.scale.set(10,10,10);
        mesh.rotateY(3.14);
        
        mesh.material[0].color.setStyle(getHexColor(currSpeed));
        exampleScene.add(mesh);  // Add the object to the scene
      });
  });
}

exLoadNextFile();     // Call callback function to start loading the objects

function exampleRender() {
  // Run the rendered
  requestAnimationFrame(exampleRender);
  ex_renderer.setClearColor(0x162521, 1);
  ex_renderer.render(exampleScene, ex_camera);
}
exampleRender();  // Begin loop to run the renderer