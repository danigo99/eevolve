var populations = []; // Array holds the list of populations which will be added to the environment

var creatures = []; // Array holds the list of creatures

var nameIndex = 1;

var currName;

var amtFood;

/**
* @desc Updates the list of populations in the populationsList page
* @param void.
* @return void
*/
function updatPopsList(){
  var wholeString = '';
  var p;
  for (p=0;p<populations.length;p++){
    wholeString = wholeString + '<div class="container populationHolder" id="container'+populations[p].getName()+'"><div class="row"><div class="col-sm-10"><span class="align-middle cardTitle">' + populations[p].getName() + '</span></div><div class="col-sm-1"><img src="../imgs/edit.png" id="editImg" class="pull-right" width="40px" onclick="editPopulation(&quot;' + populations[p].getName() + '&quot;)" data-toggle="tooltip" title="Edit"></div><div class="col-sm-1"><img src="../imgs/delete.png" id="delImg" class="pull-right" width="40px" onclick="deletePopulation(&quot;' + populations[p].getName() + '&quot;)" data-toggle="tooltip" title="Delete"></div></div></div>'
  }
  
  document.getElementById("listHolder").innerHTML = wholeString;
}

/**
* @desc Called when the "Add a population" button is pressed on the populationsList page
* @param void.
* @return void
*/
function openNewPopulation(){
  document.getElementById("populationNameHolder").innerHTML = "Population "+nameIndex;
  
  document.getElementById("editFlag").value = "0";
  
  document.getElementById("plusImg").src = "../imgs/plus.png";
  
  
  document.getElementById("populationsList").style.display = "none";
  document.getElementById("simCreation").style.display = "block";
}

/**
* @desc Called when the edit button is pressed on a specific population in the populationsList page
* @param name - the name of the population to be edited
* @return void
*/
function editPopulation(name){
  document.getElementById("populationNameHolder").innerHTML = name;
  currName = name;
  
  document.getElementById("editFlag").value = "1";
  
  document.getElementById("plusImg").src = "../imgs/save.png";
  
  for (var p=0;p<populations.length;p++){
    if (populations[p].getName()==name){
      document.getElementById("populationSizeBox").value = populations[p].getPopSize();
      document.getElementById("speedSlider").value = populations[p].getSpeed();
      document.getElementById("sizeSlider").value = populations[p].getSize();
      document.getElementById("sightSlider").value = populations[p].getSight();
      break;
    }
  }
  updateParams();
  
  document.getElementById("survivalNeedsLabel").innerHTML = "Daily food needed to survive: "+(calcFoods()/2);
  document.getElementById("reproduceNeedsLabel").innerHTML = "Daily food needed to reproduce: "+(calcFoods());
  
  document.getElementById("populationsList").style.display = "none";
  document.getElementById("simCreation").style.display = "block";
}

/**
* @desc Called when the delete button is pressed on a specific population in the populationsList page
* @param name - the name of the population to be deleted
* @return void
*/
function deletePopulation(name){
  var p;
  var newPops = [];
  for (p=0;p<populations.length;p++){
    if (populations[p].getName() != name){
      newPops.push(populations[p]);
    }
  }
  populations = newPops;
  
  // for (var d=0;d<document.getElementById("listHolder").children.length;d++){
 //     if (document.getElementById("listHolder").children[d].id == "container"+name){
 //       document.getElementById("listHolder").children[d].style.background = "red";
 //     }
 //   }
  
  updatPopsList();
  
  if (populations.length == 0){
    document.getElementById("startSimButton").disabled=true;
    document.getElementById("listHolder").innerHTML = '<div><h3 class="text-center" style="margin-left:auto; margin-right:auto; margin-top:50px; width:500px">Please add a population before starting a simulation</h3></div>';
  }
}

/**
* @desc Called when the + button is pressed after the user is done creating the new population in the simCreation page
* @param void.
* @return void
*/
function addPopulation(){
  var flag = document.getElementById("editFlag").value;
  if (flag == "0"){
    // Saving a new population
    var name = "Population "+nameIndex;
    var currPopSize = document.getElementById("populationSizeBox").value;
    if (currPopSize == ""){
      currPopSize = 5;
    }
    populations.push(new Population (name,currPopSize,currSpeed,currSize,currSight));
    document.getElementById("startSimButton").disabled=false;
    nameIndex++;
  
    updatPopsList();
  }else{
    // Saving a population edit
    for (var p=0; p<populations.length; p++){
      if (populations[p].getName()==currName){
        populations[p].setSize(currSize);
        populations[p].setSight(currSight);
        populations[p].setSpeed(currSpeed);
        populations[p].setPopSize(document.getElementById("populationSizeBox").value);
        break;
      }
    }
  }
  
  document.getElementById("simCreation").style.display = "none";
  document.getElementById("populationsList").style.display = "block";
}

/**
* @desc Called when the "Start Simulation" button is pressed on the populationsList page
* @param void.
* @return void
*/
function startSimulation(){
  amtFood = document.getElementById("numFoodsBox").value;
  
  var dis = document.getElementById("distSelectionBox").value;
  if (dis == "normal"){
    normalDist = true;
  }else{
    normalDist = false;
  }
  
  var sumCreatures = 0;
  for (var p=0;p<populations.length;p++){
    for (var c=0; c<populations[p].getPopSize(); c++){
      var chance = Math.random();
      chance = ((chance < 0.5) ? 0 : 1);
      var ve = Math.random();
      ve = ((ve < 0.5) ? -1 : 1);
      var ve2 = Math.random();
      ve2 = ((ve2 < 0.5) ? -1 : 1);
      var coords;
    
      if (chance==0){
        coords = [5.4*ve , ve2*(Math.random()*5.4)];
      }else{
        coords = [ve2*(Math.random()*5.4) , 5.4*ve];
      }
      creatures[sumCreatures] = new Creature ("creature"+sumCreatures, (1,2,3), populations[p].getSize(), populations[p].getSpeed(), populations[p].getSight(), 10, coords[0], coords[1]);
      sumCreatures ++;
    }
  }
  // var creatureSpacing = 10.8/(sumCreatures/4);
//   var sideCounter = 1;
//   var currStartX = 5.4;
//   var currStartZ = -5.4;
//   for (var c=0; c<sumCreatures; c++){
//     creatures[c].setStartPosX(currStartX);
//     creatures[c].setStartPosZ(currStartZ);
//     console.log(creatures[c].getName()+","+(1,2,3)+","+creatures[c].getSize()+","+creatures[c].getSpeed()+","+creatures[c].getSight()+",10,"+creatures[c].getStartPosZ()+","+creatures[c].getStartPosZ());
//     if (sideCounter==1){
//       if (eval(currStartZ)+eval(creatureSpacing) > 5.4){
//         currStartZ = eval(5.4);
//         currStartX = 5.4-creatureSpacing;
//         sideCounter ++;
//       }else{
//         currStartZ = eval(currStartZ) + eval(creatureSpacing);
//       }
//     }else if (sideCounter==2){
//       if (currStartX-creatureSpacing < -5.4){
//         currStartX = eval(-5.4);
//         currStartZ = 5.4-creatureSpacing;
//         sideCounter ++;
//       }else{
//         currStartX = eval(currStartX) - eval(creatureSpacing);
//       }
//     }else if (sideCounter==3){
//       if (currStartZ-creatureSpacing < -5.4){
//         currStartX = -5.4+eval(creatureSpacing);
//         currStartZ = eval(-5.4);
//         sideCounter ++;
//       }else{
//         currStartZ = eval(currStartZ) - eval(creatureSpacing);
//       }
//     }else if (sideCounter==4){
//       if (eval(currStartX)+eval(creatureSpacing) > 5.4){
//         currStartX = eval(5.4);
//         currStartZ = -5.4+eval(creatureSpacing);
//       }else{
//         currStartX = eval(currStartX) + eval(creatureSpacing);
//       }
//     }
//   }
  
  simGo();  //Start the simulation
  
  // Update HTML page
  document.getElementById("populationsList").style.display = "none";
  document.getElementById("mainSim").style.display = "block";
}